# Поддержка SNMPv3 с сильным шифрованием

В системе на Debian 10 библиотека net-snmp старая, где поддержка сильного шифрования не была имплементирована. 

В Debian 11 версия библиотеки уже поддерживает сильное шифрование, однако по умолчанию библиотеки собраны без такой поддержки.

Поэтому, в обоих случаях, чтобы работало snmpv3 c шифрованием необходимо и в debian 10 и в debian 11 пересобрать пакет net-snmp c его поддержкой.

В пакетах deb Glaber для этих систем не поддерживает сильное шифрование в snmp v3 так как оно редко востребовано и потребует в том числе переустановки net-snmp. 

Поэтому, для сборки потребуется и кастомная сборка net-snmp и сборка Glaber из исходных кодов.

## сборка net-snmp
Так как в debian 10 по умолчанию используется старая версия net-snmp, то туда необходимо установить net-snmp от 11 версии debian. 

Для обоих систем в файл */etc/apt/sources.list*
подключить исходники от 11 версии Debian: 
(Примечание: в случае debian 10 после установки и сборки net-snmp эти изменения вероятно, стоит откатить)
```
deb-src http://deb.debian.org/debian/ buster main
deb-src http://deb.debian.org/debian/ bullseye main
deb-src http://security.debian.org/debian-security buster/updates main
deb-src http://security.debian.org/debian-security bullseye-security main
deb-src http://deb.debian.org/debian/ buster-updates main
deb-src http://deb.debian.org/debian/ bullseye-updates main
```

Потом скачать исходные коды: 
```
apt source libsnmp40
```

Подготовить исходники к сборке: 
```
cd net-snmp-5.9+dfsg/
aptitude install devscripts build-essential lintian
```
В debian 10 исправить **debian/control** файл: там упоминается **perl-xs-dev**
его в deb10  нет, там он **libperl-dev**

В обоих системах в debian/rules в правилах конфигурации сборки пакета (39 строка)
добавить 
```
 --enable-blumenthal-aes
```


после этого собрать пакеты 

```
debuild -us -uc
```

Долее необходимо полученные пакеты установить в систему командой 
```
dpkg -i <имя файла с пакетом>
```
После того, как установлена библиотека и заголовочные файлы из получившихся пакетов, на этой системе нужно собрать Glaber, сильное шифрование будет работать автоматически.