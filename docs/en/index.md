---
slug: /
---
# Documentation (translation in progress)

## [History](general/history.md)
## [General information, functionality, system requirements](general/index.md) (TBD)
## [Glaber setup](setup/index.md) (TBD)
## [Administration](operations/index.md) (TBD)

<!-- ## Эксплуатация и системные требования
## Администрирование 
## Специфичные шаблоны 
## [Expand Glaber's capabilities](../docs/en/extending/) (TBD)
## [Usecases](../docs/en/usecases/) (TBD) -->
## [Release notes / changelog](https://gitlab.com/mikler/glaber/-/raw/master/ChangeLog.glaber)