# Cборка Glaber из исходных кодов

## Установка Golang

Необходимо установить, скачать и настроить Golang актуальной версии

[Официальное руководство по установке](https://go.dev/doc/install)

Либо можно установить вручную: 
```
   wget --no-verbose "https://golang.org/dl/go1.16.2.linux-amd64.tar.gz"
   rm -rf /usr/local/go; tar -C /usr/local -xzf go1.16.2.linux-amd64.tar.gz
   rm ./go1.16.2.linux-amd64.tar.gz
   export PATH=$PATH:/usr/local/go/bin
```

Проверить успешность установки командой 
```
go version
```

## Подготовка среды разработки (сборки)

### Установка в Debian-based системах: 
Установить пакеты: 

```
apt-get update
apt-get --ignore-missing install -y sshpass build-essential cmake libgmp3-dev gengetopt libpcap-dev flex byacc libjson-c-dev pkg-config libunistring-dev dpkg-dev devscripts wget git gcc automake dh-make build-essential autoconf autotools-dev quilt pkg-config libsnmp-dev libpq-dev libsqlite3-dev libcurl4-openssl-dev libldap2-dev libiksemel-dev libopenipmi-dev libssh2-1-dev unixodbc-dev default-jdk libxml2-dev libpcre3-dev libevent-dev curl libssl-dev

```
в Debian 10 (buster), AstraLinux, Ubuntu 18:
```
apt-get install -y libmariadb-dev default-libmysqlclient-dev libssh-dev sass
```

в Debian 11 (bullseye), Ubuntu 22:
```
apt-get install -y libmariadb-dev default-libmysqlclient-dev libssh-dev sass
```

### Установка в RHEL или rpm-based системах
Установка необходимых пакетов
```
dnf -y install dnf-plugins-core epel-release
dnf config-manager --set-enabled powertools
dnf -y install OpenIPMI-devel
dnf -y install libarchive
yum groupinstall "Development Tools" -y
yum install -y rpm-build yum-utils xz wget tar curl cmake json-c-devel gengetopt libunistring-devel libpcap-devel gmp-devel sshpass
```
## Сборка ПО

### скачать исходные коды проекта

```
git clone https://gitlab.com/mikler/glaber.git
```

### выполнить шаги по подготовке исходных кодов:
```
cd glaber
./bootstrap.sh
./configure
make dbschema gettext
autoreconf -fvi
```

Сконфигурировать необходимые компоненты 

например, для сборки компонента сервер с поддержкой postgresql можно использовать следующую команду
```
./configure --with-postgresql --with-libcurl --with-openqlssl --with-net-snmp --enable-server --enable-proxy --enable-ipv6
```

полный перечень параметрой и возможностей для сборки можно посмотреть командной
```
./configure --help
```

### Сборка
``` 
make
```

##  Основные пути расположения готовых бинарных программ
Воркеры 

```
src/glapi/
```

Пользовательский интерфейс 

```
ui/
```

Сервер, прокси, агент

```
src/zabbix_server/zabbix_server
src/zabbix_proxy/zabbix_proxy
src/zabbix_agent/zabbix_agent
```

