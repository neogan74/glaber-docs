# Экспортер логов аудита в syslog
## Описание
Экспортер можно скачать по адресу [https://gitlab.com/glaber/audit-syslog-exporter](https://gitlab.com/glaber/audit-syslog-exporter)

Экспортер позволяет отправлять новые события из лога аудита по протоколу syslog во внешние информационные системы.

Экспортер должен запускать по крону, представляет из себя php скрипт, который выбирает новые события аудита через АПИ и отсылает их через протокол syslog по одному событию в одной строке лога

Экспортер запоминает в файле время последнего экспорты и при следующем запуске экспортирует только новые записи.  Если файл не существует, экспортер сам его создаст и запишет в него текущее время. Таким образом, он начнет экспортировать данные с момента текущего запуска.

Для работы с API используется библиотека https://github.com/intellitrend/zabbixapi-php . Библиотека включена в исходные файлы, скачивать отдельно не нужно. 

### Настройка
В комплекте идет пример с файлом настройки. 
Пример: 
```
<?php declare(strict_types = 1);
    return [
        'facility' => '22', 
        'audit_ts_filename' => '/var/run/zabbix/audit_export_ts',
        'syslog_host' => '127.0.0.1',
        'syslog_port' => 514,
        'API_TOKEN' => '954f029a158133fff110b27756e8aeff42987a25b981c653d925e819dfbfda5f',
        'API_URL' => 'https://test.glaber.ru',
        'audit_filter' => [
        //    'resourcetype' => [0,1,2,3,5,6,12]
            ],
        'audit_fields' => ['username','clock', 'ip', 'action', 'resourcename', 'details'],
    ];
 ?>
```
### Назначение опций
 
 `API_TOKEN` - сгенерировать токен из под пользователя, имеющего доступ к логу аудита. 

 `API_URL` - указать адрес фронтенда сервера, где работает АПИ. Указывать имя скрипта обработчика АПИ не нужно. Поддерживаются `https` и `http` схемы
 
 `audit_ts_filename` - имя файла, где скрипт будет хранить и обновлять отметку времени, по которую был экспорт

 `syslog_host` и `syslog_port` задают адрес и порт, куда будут отправляться записи

 `audit_filter` позволяет ограничить типы событий аудита, которые будут отправляться. Соответствие идентификаторов можно посмотреть в файле CAuditExporter.php

`audit_fields` - список полей, которые будут в экспорте. Полный список: `'username','clock', 'ip', 'resourcename', 'details'`, также экспортер добавит поля action ( `login, add, remove`...) - символьное имя действия и поле resource (`user, host, group`...)-  имя типа ресурса. 

## Формат вывода
### Примеры сообщений
#### Изменение конфигурации
```
<22>Oct 23 07:40:38 glaber audit: {"username":"System","clock":"1697894707","ip":"","action":"Add","resourcename":"\u0421\u0435\u0442\u0435\u0432\u0430\u044f \u0443\u0442\u0438\u043b\u0438\u0437\u0430\u0446\u0438\u044f calid563aadec29","details":"{\"graph.graphid\":[\"add\",76309749],\"graph\":[\"add\"],\"graph.name\":[\"add\",\"\u0421\u0435\u0442\u0435\u0432\u0430\u044f \u0443\u0442\u0438\u043b\u0438\u0437\u0430\u0446\u0438\u044f calid563aadec29\"],\"graph.show_work_period\":[\"add\",0],\"graph.show_triggers\":[\"add\",0],\"graph.flags\":[\"add\",4],\"graph.gitems[132557330]\":[\"add\"],\"graph.gitems[132557330].drawtype\":[\"add\",2],\"graph.gitems[132557330].color\":[\"add\",\"FF3333\"],\"graph.gitems[132557330].yaxisside\":[\"add\",1],\"graph.gitems[132557330].calc_fnc\":[\"add\",4],\"graph.gitems[132557330].itemid\":[\"add\",1000440842],\"graph.gitems[132557331]\":[\"add\"],\"graph.gitems[132557331].drawtype\":[\"add\",2],\"graph.gitems[132557331].sortorder\":[\"add\",1],\"graph.gitems[132557331].color\":[\"add\",\"3333FF\"],\"graph.gitems[132557331].yaxisside\":[\"add\",1],\"graph.gitems[132557331].calc_fnc\":[\"add\",4],\"graph.gitems[132557331].itemid\":[\"add\",1000440836]}","resource":"Graph"}
```
#### Вход и выход из системы (пользовательские операции)
```
<22>Oct 23 08:11:56 glaber audit: {"username":"ivanov","clock":"1698030702","ip":"10.0.0.1","action":"Logout","resourcename":"","details":"","resource":"User"}
```
```
<22>Oct 23 08:11:56 glaber audit: {"username":"petrov","clock":"1698030704","ip":"10.0.0.2","action":"Login","resourcename":"","details":"","resource":"User"}
```
### Описание формата  экспорта аудита 

`<22>` - фиксированное поле приоритета, не меняется

`Oct 23 07:40:38` – время отправки сообщения аудита

`glaber` – фиксированное поле имени приложения, не меняется

`audit` – фиксированное поле с именем модуля, не меняется

`текст сообщения` - текст в формате json

### Описание формата сообщений 

Поля в сообщении фиксированные. В случае, если для события аудита поле не применимо, его значение будет пустой строкой:
`"details":""`
#### 
Список полей: 
`username, clock, ip, action, resourcename, details, resource
Пример текста сообщения:`

Примечание: для наглядности сообщение отформатировано

```
{
    "username": "System",
    "clock": "1697897455",
    "ip": "",
    "action": "Add",
    "resourcename": "\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043f\\u0435\\u0440\\u0435\\u0434\\u0430\\u043d\\u043d\\u044b\\u0445\\/\\u043f\\u043e\\u043b\\u0443\\u0447\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0430\\u043a\\u0435\\u0442\\u043e\\u0432 cali7364ece94d1",
    "details": "{\"graph.graphid\":[\"add\",76309754],\"graph\":[\"add\"],\"graph.name\":[\"add\",\"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043f\\u0435\\u0440\\u0435\\u0434\\u0430\\u043d\\u043d\\u044b\\u0445\\/\\u043f\\u043e\\u043b\\u0443\\u0447\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0430\\u043a\\u0435\\u0442\\u043e\\u0432 cali7364ece94d1\"],\"graph.show_work_period\":[\"add\",0],\"graph.show_triggers\":[\"add\",0],\"graph.flags\":[\"add\",4],\"graph.gitems[132557340]\":[\"add\"],\"graph.gitems[132557340].drawtype\":[\"add\",2],\"graph.gitems[132557340].color\":[\"add\",\"FF3333\"],\"graph.gitems[132557340].yaxisside\":[\"add\",1],\"graph.gitems[132557340].calc_fnc\":[\"add\",4],\"graph.gitems[132557340].itemid\":[\"add\",1000440856],\"graph.gitems[132557341]\":[\"add\"],\"graph.gitems[132557341].drawtype\":[\"add\",2],\"graph.gitems[132557341].sortorder\":[\"add\",1],\"graph.gitems[132557341].color\":[\"add\",\"3333FF\"],\"graph.gitems[132557341].yaxisside\":[\"add\",1],\"graph.gitems[132557341].calc_fnc\":[\"add\",4],\"graph.gitems[132557341].itemid\":[\"add\",1000440862]}",
    "resource": "Graph"
}
```

### Описание полей в тексте  сообщения

`username` -  имя пользователя, от которого изменение производилось. В случае, если изменение было произведено самой системой, имя пользователя будет «System»

`clock` - время в формате UNIX timestamp с точностью до секунд
ip - IP адрес клиентской сессии, с которой было произведено изменение. Для изменений, вызванных системой, поле будет пустым

`action` - Тип события или изменения. Возможные значения: 
`"Login", "Failed login", "Logout", "Add", "Update", "Delete", "Execute", "Configuration refresh"`

`resource` - Тип изменившегося ресурса. Возможные значения:
`"Action", "API token", "Authentication", "Autoregistration", "Connector", "Event correlation", "Dashboard", "Discovery rule", ”Graph", "Graph prototype", "High availability node", "Host", "Host group", "Host prototype", "Housekeeping", "Icon mapping", "Image", "Service", "Item" ,"Item prototype", "Macro" ,"Maintenance" ,"Map", “Media type", "Module", "Proxy", "Regular expression",  "Web scenario", "Scheduled report", "Script", "Settings", "SLA", "Template", "Template dashboard", "Trigger", "Trigger prototype", "Template group", "User", "User directory", "User group", "User role", "Value map"`

`resourceid` -числовой (uint64) идентификатор изменившегося ресурса.

`details` - Описание произведённых изменений. Это Json массив с описанием изменившихся полей объекта с указанием действия в формате: имя поля или объекста - действие Возможные действия: 
`Add, Update, Delete`

Если в действии присутствуют значения, то указываются эти значения. Если значение меняются, указываются и старое, и новое значения

#### Пример 1: (частичный) – добавление объекта «График»
````
{
"graph":["add"],
"graph.graphid":["add",76309754],
"graph.name":["add","test graph value"], "graph.show_work_period":["add",0],
"graph.flags":["add",4] ",
... 
}
````

#### Пример 2 – изменение имени узла и видимого имени узла с test1234 на test:
````
{
"host.host":["update","test1234","test"],
"host.name":["update","test1234","test"]
}
````

`resourcename` – имя ресурса

Примечание: для текстовых значений используется кодировка UTF, поэтому кириллические имена, названия, значения изменившихся полей в логе будут нечитаемыми 

