# АPI зависимостей узлов **hostdepends**

## Описание
Объект **hostdepends** представляет собой [связь между узлами](../dependencies/index.md).



## Свойства
Содержит сдедующие поля

| Имя поля | Тип | Описание |
|:---------|-----|----------------------
| depid | uint64 |Уникальный идентификатор связи
| hostid_up | uint64 |Hostid узла по направлению в сторону предоставления услуги
| hostid_down | uint64 | Hostid узла по направлению в сторону пользователя или другого потребителя услуги
| name | string | Имя услуги или топологии услуги

## Примеры использования

### Создание связи: hostdepends.create

**Важно**: В данный момент отсутствует проверка на уникальность связи

Запрос:
```
POST https://example.glaber.ru/api_jsonrpc.php
Content-Type: application/json-rpc
Authorization: Bearer __token__

{
    "jsonrpc": "2.0",
    "method": "hostdepends.create",
    "params": {
        "hostid_up": 149664,
        "hostid_down": 160654,
        "name": "topology_bgp"
    },
    "id": 1
}
```

Ответ:
```
{
   "jsonrpc": "2.0",
   "result": {
        "depid": "3"
    },
    "id": 1
}
   
```

### Массовое создание связей

**Важно**: В данный момент отсутствует проверка на уникальность связи

Запрос:
```
POST https://example.glaber.ru/api_jsonrpc.php
Content-Type: application/json-rpc
Authorization: Bearer __token__

{
    "jsonrpc": "2.0",
    "method": "hostdepends.masscreate",
    "params": [
        {"hostid_up": 149664, "hostid_down": 157085, "name": "test"},
        {"hostid_up": 149664, "hostid_down": 157233, "name": "test"},
        {"hostid_up": 149664, "hostid_down": 157937, "name": "test"}
    ],
    "id": 1
}
```

Ответ:
```
{
  "jsonrpc": "2.0",
  "result": {
    "depids": [
      "11",
      "12",
      "13"
    ]
  },
  "id": 1
}
```

### Получение информации о связи: hostdepends.get

Поддерживаются значения фильтра: 

|Имя|Тип|Значение
|:--|---|------
|filter.hostid_up| uint64 | выбрать связи, где узел, смотрящий направлении up, равен hostid_up
|filter.hostid_down| uint64 | выбрать связи, где узел, смотрящий в сторону down, равен hostid_down
|filter.name| string | выбрать связи, где имя сервися совпадает с name
|hostid| uint64 | выбрать связи, где узел в может быть с любой стороны связи
|with_hostname| bool | добавить к выводу имена узлов

Запрос:
```
POST https://example.glaber.ru/api_jsonrpc.php
Content-Type: application/json-rpc
Authorization: Bearer __token__

{
    "jsonrpc": "2.0",
    "method": "hostdepends.get",
    "params": {
        "filter": {
             "hostid_up": 149664
        }
    },
    "id": 2
}
```

Ответ:
```
{
  "jsonrpc": "2.0",
  "result": [
    {
      "hostname_up": "agg1-chel2.is74.ru",
      "hostname_down": "camback8-chel1.is74.ru",
      "depid": "5",
      "hostid_up": "149664",
      "hostid_down": "157085",
      "name": "rrrrrrr"
    },
    {
      "hostname_up": "agg1-chel2.is74.ru",
      "hostname_down": "camback8-chel1.is74.ru",
      "depid": "8",
      "hostid_up": "149664",
      "hostid_down": "157085",
      "name": "rrrrrrr"
    },
    {
      "hostname_up": "agg1-chel2.is74.ru",
      "hostname_down": "camback9-chel2.is74.ru",
      "depid": "6",
      "hostid_up": "149664",
      "hostid_down": "157233",
      "name": "rrrrrrr"
    },
    {
      "hostname_up": "agg1-chel2.is74.ru",
      "hostname_down": "camback9-chel2.is74.ru",
      "depid": "9",
      "hostid_up": "149664",
      "hostid_down": "157233",
      "name": "rrrrrrr"
    }
  ],
  "id": 2
}
```
### Обновить связь: hostdepends.update

Запрос
```
POST https://example.glaber.ru/api_jsonrpc.php
Content-Type: application/json-rpc
Authorization: Bearer __token__

{
    "jsonrpc": "2.0",
    "method": "hostdepends.update",
    "params": {
        "depid": "9",
        "hostid_up": 160654,
        "hostid_down": 149664,
        "name": "rrrrrrr"
    },
    "id": 1
}
```

Ответ
```
```

### Удалить связь:  hostdepends.update

Запрос
```
POST https://example.glaber.ru/api_jsonrpc.php
Content-Type: application/json-rpc
Authorization: Bearer __token__

{
    "jsonrpc": "2.0",
    "method": "hostdepends.delete",
    "params": [
        "4","5","5555"
    ],
    "id": 1
}
```

Ответ
```
```
