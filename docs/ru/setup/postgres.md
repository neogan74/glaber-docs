# Glaber and Postgresql

Подключаемся к базе данных главным пользователем
```
psql -h 10.11.12.13 -W postgres -d postgres -W 
```
Вводим пароль.

Создаем пользователя:
```
create user glaber with password '12345';
```
и создаем базу для пользователя

```
create database glaber owner=glaber;
```

Подключаемся к базе с этим пользователем для проверки связи
```
psql -h 10.11.12.13 -W glaber -d glaber -W 
```

Записываем структуры и первоначальные данные в базу
(Файлы структуры и данных для базы данных поставляются с дистрибутивом glaber, либо нужно ее сгерерировать из исходного кода)
```
zcat schema.sql | psql -h 10.11.12.13 -W glaber -d glaber -W  
```

Дальше данные и картинки
```
zcat data.sql | psql -h 10.11.12.13 -W glaber -d glaber -W
zcat images.sql | psql -h 10.11.12.13 -W glaber -d glaber -W  
```

Далее переименовываем схему с дефолтной и меняем владельца
```
alter schema public rename to glaber;
alter schema glaber owner to glaber;
```
Запускаем glaber-server и убеждаемся по логу что все работает
```
systemctl start glaber-server
tail -n50 /var/log/zabbix_server.log
```
